#!/usr/bin/env bash

source /usr/lib/epkg/libepkg

checkroot
checksystemmode

action=$1
shift

case "$action" in
	install)
		checksync
		emerge -av "$@"
		;;
	install-no-collision)
		checksync
		FEATURES="parallel-fetch -collision-protect -protect-owned candy compressdebug splitdebug" emerge -av "$@"
		;;
	install-bin-pkg)
		checksync
		EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS} --getbinpkg " emerge -av "$@"
		;;
	remove)
		checksync
		emerge --depclean -av "$@"
		;;
	backuppkg)
		PKGDIR="${PKGDIR:-}" backuppkg "$@"
		;;
	forceremove)
		checksync
		emerge --unmerge -av "$@"
		;;
	clean)
		checksync
		emerge --depclean -av
		;;
	cleanoutdated)
		cleanoutdated
		;;
	cleanlocalpkg)
		cleanlocalpkg
		;;
	upgrade)
		checksync
		emerge -uDvaN @system @world "$@"
		;;
	upgrade-full)
		checksync
		emerge -uDvaN --with-bdeps=y @system @world "$@"
		;;
        upgrade-specific)
                checksync
                emerge -uDvaN "$@"
		;;
        upgrade-nocol)
                checksync
                FEATURES="parallel-fetch -collision-protect -protect-owned candy compressdebug splitdebug" emerge -uDvaN --with-bdeps=y @system @world "$@"
                ;;
	autoinstall)
		argentsync
		emerge -v "$@"
		;;
	autoinstall-no-collision)
		argentsync
		FEATURES="parallel-fetch -collision-protect -protect-owned candy compressdebug splitdebug" emerge -v "$@"
		;;
	autoinstall-bin-pkg)
		argentsync
		EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS} --getbinpkg " emerge -v "$@"
		;;
	autoremove)
		argentsync
		emerge --depclean -v "$@"
		;;
	reinstall-depend)
		reinstall-depend "$@"
		;;
	backupallpkg)
		PKGDIR="${PKGDIR:-}" backupallpkg "$@"
		;;
	recompilepkg)
		recompilepkg "$@"
		;;
	recompileallpkg)
		argentsync
		cleanoutdated
		recompileallpkg
		;;
	recompile-depend)
		recompile-depend "$@"
		;;
	recompile-depend-new)
		recompile-depend-new "$@"
		;;
	src-newuse)
		argentsync
		src-newuse "$@"
		;;
	buildpkgonly)
		buildpkgonly "$@"
		;;
	buildinstall)
		PKGDIR="${PKGDIR:-}" buildinstall "$@"
		;;
	autobuildinstall)
		PKGDIR="${PKGDIR:-}" autobuildinstall "$@"
		;;
	autoreinstall-depend)
		autoreinstall-depend "$@"
		;;
	autorecompilepkg)
		argentsync
		autorecompilepkg "$@"
		;;
	autorecompile-depend)
		autorecompile-depend "$@"
		;;
	autorecompile-depend-new)
		cleanoutdated
		autorecompile-depend-new "$@"
		;;
	autorecompileallpkg)
		argentsync
		cleanoutdated
		autorecompileallpkg
		;;
	autoforceremove)
		argentsync
		emerge --unmerge -v "$@"
		;;
	autoclean)
		argentsync
		emerge --depclean -q
		;;
	autobuildpkgonly)
		argentsync
		cleanoutdated
		autobuildpkgonly "$@"
		;;
	autoupgrade)
		argentsync
		emerge -uDvN @system @world "$@"
		;;
	autoupgrade-full)
		argentsync
		emerge -uDvN --with-bdeps=y @system @world "$@"
		;;
	autoupgrade-nocol)
		argentsync
		FEATURES="parallel-fetch -collision-protect -protect-owned candy compressdebug splitdebug" emerge -uDvN --with-bdeps=y @system @world "$@"
		;;
	upgrade-specific)
		checksync
		emerge -uDvN "$@"
		;;
	remove-orphans)
		remove-orphans
		;;
	fetchsourcepkg)
		fetchsourcepkg "$@"
		;;
	fetchbinpkg)
		fetchbinpkg "$@"
		;;
	search)
		emerge -s "$@"
		;;
	update)
		argentsync
		;;
	sysinfo)
		emerge --info
		;;
	*)
		cat <<-"EOH"
			Usage: epkg command [package(s)]

			epkg is a simple wrapper around portage, gentoolkit, and portage-utils that provides an
			apt-get/yum-alike interface to these commands, to assist people transitioning from
			Debian/RedHat-based systems to Gentoo.

			Commands :
				install - Install new packages
				remove - Remove packages safely
				backuppkg - Backup specific packages with own configurations for later use
				forceremove - *Unsafely* remove packages
				clean - Remove packages that are no longer needed
				cleanoutdated - Remove local saved and outdated packages
				cleanlocalpkg - Remove all locally saved package ( including index ) usually in /usr/portage/packages

				upgrade -  Upgrade system. No build dependencies are upgraded
				upgrade-full - Upgrade system. All build dependencies included for upgrade
				upgrade-nocol - Upgrade system without collision protection
				upgrade-specific - Upgrade specific package(s)
				install-no-collision - Install a package forcefully without collision protection
				install-bin-pkg - Install only a precompiled binary package if available

				autoinstall - Install new packages (no confirmation)
				autoinstall-no-collision - Install new package forcefully without collision protection ( no confirmation )
				autoinstall-bin-pkg - Install only a precompiled binary package if available ( no confirmation )
				autoremove - Remove packages safely (no confirmation)
				reinstall-depend - Reinstall all dependencies of given package(s) based on current version/revision
				src-newuse - Check a specific package if it has to be reinstalled due to USE flag change ( portage-specific )

				backupallpkg - Backup the whole system. Usually in /usr/portage/packages
				recompilepkg - Recompile and install packages with self-defined options ( man portage )
				recompileallpkg - Recompile, install and save all the packages locally ( usually in /usr/portage/packages/ )
				recompile-depend - Compile/Recompile all currently installed packages that depend on given package(s)
				recompile-depend-new - Compile/Recompile all newly installed packages that depend on given package(s)
				buildpkgonly - Build/Recompile package(s) and export them locally ( usually in /usr/portage/packages )
				buildinstall - Recompile/Build package(s) and install them immediately ( usually in /usr/portage/packages )

				autoupgrade - Upgrade system (no confirmation). No build dependencies are upgraded
				autoupgrade-full - Upgrade system ( no confirmation ). All build dependencies included for upgrade
				autoupgrade-nocol - Upgrade system without collision protection ( no confirmation )
				autoupgrade-specific - Upgrade specific package(s) ( no confirmation )

				autoreinstall-depend - Reinstall all dependencies of given package(s) based on current version/revision ( no confirmation )
				autorecompilepkg - Recompile packages with self-defined options ( no confirmation )
				autorecompileallpkg - Recompile and save all the packages in the system ( no confirmation )
				autorecompile-depend - Compile/Recompile all currently installed packages that depend on given package(s) ( no confirmation )
				autorecompile-depend-new - Compile/Recompile all newly-installed packages that depend on given package(s) ( no confirmation )
				autoforceremove - *Unsafely* remove packages (no confirmation)

				autoclean - Remove packages that are no longer needed (no confirmation)
				autobuildpkgonly - Build/Recompile package(s) and export them locally ( no confirmation )
				autobuildinstall - Build/Recompile package(s) and autoinstall them immediately ( no confirmation )

				remove-orphans - Remove orphaned packages ( with confirmation )
				fetchbinpkg - Downloads only binaries from remote
				fetchsourcepkg - Downloads the source of the program. Usually in /usr/portage/distfiles/
				search - Search for packages
				update - Resync portage tree, portage config && argent-ws overlay
				sysinfo - Display information about installed core packages and portage configuration
		EOH
		;;
esac
